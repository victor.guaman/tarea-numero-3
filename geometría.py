#TAREA NUMERO 3
#Cree un modulo llamado "geometria" Y EN EL REALICE LO SIGUIENTE:
#Crear una clase llamada Punto con sus dos coordenadas X e Y.
#Añadir un método constructor para crear puntos fácilmente. Si no se reciben una coordenada,su valor será cero.
#Sobreescribir el método __str__, para que al imprimir por pantalla un punto aparezca en formato (X,Y)
#Añadir un método llamado cuadrante que indique a qué cuadrante pertenece el punto, teniendo en cuenta
#que si X == 0 e Y != 0 se sitúa sobre el eje Y, si X != 0 e Y == 0 se sitúa sobre el eje X y si X == 0
#Y == 0 está sobre el origen.
#Añadir un método llamado vector, que tome otro punto como parámetro y calcule el vector resultante
#entre los dos puntos.
#Añadr un método llamado distancia, que tome otro punto como parámetro y calcule la distancia entre
#los dos puntos y la devuelva. Recuere la fórmula:
#Crear una clase llamada Rectangulo con dos puntos (inicial y final) que formarán la diagonal del
#rectángulo.
#Añadir al rectángulo un método llamado base que devuelva la base.
#Añade al rectángulo un método llamado altura que devuelva la altura.
#Añade al rectángulo un método llamado area que devuelva el area."""
#AUTOR=VICTOR GUAMAN
import math
#creo una clase llamada Punto
class Punto:
#defino dos puntos x y y.#
    x=int
    y=int

    def __init__(self, x=0 , y=0 ):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)
#creo un metodo llamado cuadrante.
    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} pertenece al primer cuadrante".format(self))

        elif self.x < 0 and self.y > 0:
            print("{} es perteneciente al segundo cuadrante".format(self))

        elif self.x < 0 and self.y < 0:
            print("{} es perteneciente al tercer cuadrante".format(self))

        elif self.x > 0 and self.y < 0:
            print("{} es perteneciente  al cuarto cuadrante".format(self))

        elif self.x != 0 and self.y == 0:
            print("{} se situa sobre el eje X".format(self))

        elif self.x == 0 and self.y == 0:
            print("{} se situa en el origen ".format(self))
#defino un metodo llamado vector.
    def vector(self, p):
        print("El  valor del vector resultante de: {} y el : {} es  igual a: ({}, {})".format(
            self, p, p.x - self.x, p.y - self.y) )
#defino un metodo llamado distancia.
    def distancia(self, p):
        d = math.sqrt((p.x - self.x)**2 + (p.y - self.y)** 2)
        print("La distancia total entre los puntos {} y {} es {}".format(self, p, d))

class Rectangulo:

    def __init__(self, inicial=Punto(), final=Punto()):
        self.inicial = inicial
        self.final = final

        self.base = abs(self.final.x - self.inicial.x)
        self.altura = abs(self.final.y - self.inicial.y)
        self.area = self.base * self.altura

    def base(self):
        print("La base del rectángulo es {}".format( self.base ) )

    def altura(self):
        print("La altura del rectángulo es {}".format( self.altura ) )

    def area(self):
        print("El área del rectángulo es {}".format( self.area ) )

punta = Punto(2,3)
puntb = Punto(5,5)
puntc = Punto(-3, -1)
puntd = Punto(0,0)

punta.cuadrante()
puntc.cuadrante()
puntd.cuadrante()

punta.vector(puntb)
puntb.vector(punta)

puntc.distancia(punta)
puntb.distancia(puntb)

punta.distancia(puntd)
puntd.distancia(puntd)

re = Rectangulo(A, B)
re.base()
print(re.base)
re.altura()
print(re.altura)
re.area()
print(re.area)